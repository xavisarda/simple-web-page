<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <h1>It's working!</h1>
    <ul>
    //Bucle 1
    <?php for($i = 0 ; $i < 10 ; $i++) { ?>
        <li><?=$i?></li>
    <?php } ?>
    //Bucle 2
    <?php for($i = 0 ; $i < 10 ; $i++) { 
        echo '<li>'.$i.'</li>';
    } ?>
    </ul>
    <div>Div sample</div>
  </body>
</html>
